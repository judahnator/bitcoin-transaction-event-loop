Bitcoin Transaction Event Loop
==============================

This library is meant to provide a simple interface between your 
application and the blockchain.info websocket API.

The features of this library are fairly straightforward. You can do the 
following:
* Listen for activity for a certain address
* Listen for new blocks
* Listen for all new unconfirmed transactions