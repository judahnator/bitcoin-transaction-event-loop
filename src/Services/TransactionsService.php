<?php

namespace judahnator\BitcoinTransactionEventLoop\Services;


use judahnator\BitcoinTransactionEventLoop\Exceptions\TransactionStreamException;
use WebSocket\BadOpcodeException;

final class TransactionsService extends WebSocketService
{

    public function __construct()
    {
        parent::__construct('wss://ws.blockchain.info/inv');
    }

    /**
     * @throws TransactionStreamException
     */
    public function run(): void
    {
        try {
            parent::run();
        } catch (\Exception $e) {
            throw new TransactionStreamException(
                $e->getMessage(),
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * @throws BadOpcodeException
     */
    public function loopSetup(): void
    {
        $this->sendOpCode('unconfirmed_sub');
    }

    /**
     * @throws BadOpcodeException
     */
    public function loopTeardown(): void
    {
        $this->sendOpCode('unconfirmed_unsub');
    }

}