<?php

namespace judahnator\BitcoinTransactionEventLoop\Services;


use judahnator\WebsocketEventLoop\WebsocketEventLoop;
use WebSocket\BadOpcodeException;

class WebSocketService extends WebsocketEventLoop
{

    protected $wssClient;

    public function __construct(string $endpointURL, array $options = [])
    {
        parent::__construct($endpointURL, $options);

        // We need to validate some data before continuing
        $this->beforeEvent(function(): bool
        {
            return !is_null($this->getEventCallbackArgument());
        });
    }

    /**
     * We must overwrite the parent function.
     * Otherwise the extending classes will receive the json string instead of the decoded value.
     *
     * @return mixed
     */
    public function getEventCallbackArgument()
    {
        $json = parent::getEventCallbackArgument();

        if (!$json) {
            // no json, don't bother
            return null;
        }

        $message = json_decode($json);

        if (!property_exists($message, 'x')) {
            // invalid json format, continue
            return null;
        }

        return $message;
    }

    /**
     * @param string $message
     * @throws BadOpcodeException
     */
    protected function sendOpCode(string $message, array $additionalData = []) {
        $this->sendMessage(json_encode(['op' => $message] + $additionalData));
    }
}