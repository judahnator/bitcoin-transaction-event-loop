<?php

namespace judahnator\BitcoinTransactionEventLoop\Services;


final class BlocksService extends WebSocketService
{

    public function __construct()
    {
        parent::__construct('wss://ws.blockchain.info/inv', ['timeout' => 7200]);
    }

    /**
     * @throws \WebSocket\BadOpcodeException
     */
    public function loopSetup(): void
    {
        $this->sendOpCode('blocks_sub');
    }

    /**
     * @throws \WebSocket\BadOpcodeException
     */
    public function loopTeardown(): void
    {
        $this->sendOpCode('blocks_unsub');
    }
}