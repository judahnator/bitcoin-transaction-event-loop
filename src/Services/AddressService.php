<?php

namespace judahnator\BitcoinTransactionEventLoop\Services;

use WebSocket\BadOpcodeException;


/**
 * Class AddressService
 *
 * @property-read string $hash160
 * @property-read string $address
 * @property-read int $n_tx
 * @property-read int $total_received
 * @property-read int $total_sent
 * @property-read int $final_balance
 * @property-read array $txs
 *
 * @package judahnator\BitcoinTransactionEventLoop\Services
 */
final class AddressService extends WebSocketService
{

    private $addressInfo;

    public function __construct($address)
    {
        $remoteResponse = file_get_contents("https://blockchain.info/rawaddr/{$address}");
        $this->addressInfo = json_decode($remoteResponse);

        if (json_last_error() !== JSON_ERROR_NONE && $remoteResponse) {
            throw new \InvalidArgumentException("Could not read from remote endpoint: {$remoteResponse}");
        }

        parent::__construct('wss://ws.blockchain.info/inv', ['timeout' => 7200]);
    }

    public function __get($name)
    {
        if (property_exists($this->addressInfo, $name)) {
            return $this->addressInfo->$name;
        }
        return null;
    }

    /**
     * Prepare the application.
     * @throws BadOpcodeException
     */
    public function loopSetup(): void
    {
        $this->sendOpCode('addr_sub', ['addr' => $this->address]);
    }

    /**
     * Loop has finished, destruct the application
     * @throws BadOpcodeException
     */
    public function loopTeardown(): void
    {
        $this->sendOpCode('addr_unsub', ['addr' => $this->address]);
    }
}